import { faker } from '@faker-js/faker';
import { ILocation } from './types';

class User implements ILocation {
  declare name: string;
  declare location: {
    lat: number;
    lng: number;
  };
  constructor() {
    this.name = faker.person.fullName();
    this.location = {
      lat: faker.location.latitude(),
      lng: faker.location.longitude(),
    };
  }

  markerContent(): string {
    return `
    <div>
      <h3>Username : ${this.name}</h3>
      <ul>Location:
        <li>Latitude : ${this.location.lat}</li>
        <li>Lngitude : ${this.location.lng}</li>
      </ul>
    </div>
    `;
  }
}

export default User;
