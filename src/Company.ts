import { faker } from '@faker-js/faker';
import { ILocation } from './types';

class Company implements ILocation {
  declare companyName: string;
  declare catchPharse: string;
  declare location: {
    lat: number;
    lng: number;
  };

  constructor() {
    this.companyName = faker.company.name();
    this.catchPharse = faker.company.catchPhrase();
    this.location = {
      lat: faker.location.latitude(),
      lng: faker.location.longitude(),
    };
  }

  markerContent(): string {
    return `
    <div>
      <h3>Company name : ${this.companyName}</h3>
      <p>Company slogan : ${this.catchPharse}</p>
      <ul>Location:
        <li>Latitude : ${this.location.lat}</li>
        <li>Lngitude : ${this.location.lng}</li>
      </ul>
    </div>
    `;
  }
}

export default Company;
