import { ILocation } from './types';

export default class MapCustomer {
  private declare googleMap: google.maps.Map;
  constructor(element: HTMLElement) {
    this.googleMap = new google.maps.Map(element, {
      zoom: 1,
      center: { lat: 0, lng: 0 },
    });
  }

  addMarker(mapable: ILocation): void {
    const marker = new google.maps.Marker({
      map: this.googleMap,
      position: {
        lat: mapable.location.lat,
        lng: mapable.location.lng,
      },
    });

    marker.addListener('click', () => {
      const infoWindow = new google.maps.InfoWindow({
        content: mapable.markerContent(),
      });

      infoWindow.open(this.googleMap, marker);
    });
  }
}
