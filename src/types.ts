interface ILocation {
  location: {
    lat: number;
    lng: number;
  };
  markerContent: () => string;
}

export { ILocation };
