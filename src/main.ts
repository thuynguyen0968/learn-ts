import Company from './Company';
import MapCustomer from './Map';
import User from './User';

const company = new Company();
const user = new User();

const mapElement = document.getElementById('map');
const mapApp = new MapCustomer(mapElement as HTMLElement);
mapApp.addMarker(user);
mapApp.addMarker(company);
